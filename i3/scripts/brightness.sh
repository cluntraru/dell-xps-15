#!/bin/bash

brightness_path="/sys/class/backlight/intel_backlight/brightness"
brightness=$(cat $brightness_path)
brightness_new=$(($brightness + $1))

if [ $brightness_new -le 1500 ] && [ $brightness_new -ge 0 ]; then
    sh -c "echo $(($brightness + $1)) > $brightness_path"
elif [ $brightness_new -lt 0 ]; then
    sh -c "echo 1 > $brightness_path"
fi

