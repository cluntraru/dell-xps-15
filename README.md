# Dell XPS 15
Do not use kernel 4.11 or above (also tested with 5+)

## Debian

#### Bluetooth
Copy BCM-0a5c-6410.hcd to /lib/firmware/brcm/ for bluetooth to work

#### WiFi
Get brcmfmac driver - instructions on Debian wiki

#### nVidia
Bumblebee (nouveau version tested) - instructions on Debian wiki

#### i3 Config
For brightness keys to work properly, disable sudo password for scripts/brightness.sh

#### i3blocks 
https://github.com/vivien/i3blocks
https://github.com/vivien/i3blocks-contrib
https://github.com/Anachron/i3blocks

#### LightDM 
/etc/lightdm/lightdm-gtk-greeter.conf
/etc/lightdm/lightdm.conf

#### Dependencies
xss-tool
lightdm
light-locker
i3blocks + all dependencies for i3blocks

#### GRUB
sudo grub-mkconfig -o /boot/grub/grub.cfg
GRUB_FONT=/path/to/font (after grub-mkfont)
GRUB_BACKGROUND=/path/to/picture

#### Font
FiraMono-Regular (both grup and urxvt)

#### GTK
Theme: Materia-dark
Lightdm: lightdm-gtk-greeter.conf

#### Background
Nitrogen

